const express = require ("express");

// localhost port
const port = 3000;

// Creating Server
const app = express();



app.use(express.json());



let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];


app.get("/",(request, response)=>{

	response.send("Welcome to the Home Page")

});


app.get("/items",(request,response)=>{

	response.send(items);


});

app.delete("/delete-items" ,(request ,response)=>{

	let deleteItems = items.pop();

	response.send(deleteItems);

})





app.listen(port,() => console.log(`Running Server at port ${port}`));